unit main;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, pqconnection, sqldb, db, FileUtil, Forms,
  Controls, Graphics, Dialogs, StdCtrls, ExtCtrls, Buttons, ComCtrls, Spin,
  csvreadwrite;

type

  State =(DISCONNECTED, READY, RUNNING, PAUSED);

  { TfrmMain }

  TfrmMain = class(TForm)
    btnConnect: TSpeedButton;
    ckExcludeSelf: TCheckBox;
    comboLogFiles: TComboBox;
    comboRefreshInterval: TComboBox;
    edHost: TLabeledEdit;
    edUser: TLabeledEdit;
    edPassword: TLabeledEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    listView: TListView;
    Panel1: TPanel;
    DBConnection: TPQConnection;
    btnStart: TSpeedButton;
    btnClear: TSpeedButton;
    sePort: TSpinEdit;
    SQLQuery1: TSQLQuery;
    SQLTransaction1: TSQLTransaction;
    statusBar: TStatusBar;
    timer: TTimer;
    procedure btnClearClick(Sender: TObject);
    procedure btnStartClick(Sender: TObject);
    procedure comboRefreshIntervalChange(Sender: TObject);
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure timerTimer(Sender: TObject);
    procedure toggleConnection(Sender: TObject);
  private
    myState:State;
    lastBytesRead:integer;
    parser: TCSVParser;
    firstRun:boolean;
    procedure setState(newState:State);
    procedure clearList;
    procedure getLogRecords;
    function populateFileList:boolean;
    procedure setTimerInterval;
    procedure setValues;
    procedure emptyLogCombo;
  public
    { public declarations }
  end;

var
  frmMain: TfrmMain;

implementation

{$R *.lfm}

uses
  loginfo;

const
  APPLICATION_NAME='pgprofiler';
  MAX_FILE_SIZE=100000000;   //todo: using postgres bigint max size gives error??
  MAX_LOG_LINES=500;

function TfrmMain.populateFileList:boolean;
var
  logFilesExist:boolean;
  sql:String;
begin
  logFilesExist:=false;
  //empty of any previously retreived file info
  emptyLogCombo;
  SQL := 'SELECT f.filename, s.size FROM pg_ls_dir(''pg_log'') AS f(filename), LATERAL pg_stat_file(''pg_log/'' || f.filename) as s where f.filename like ''%.csv'' order by f.filename desc';
  SQLQuery1.SQL.Text:= SQL;
  SQLTransaction1.StartTransaction;
  SQLQuery1.Open;
  while not SQLQuery1.EOF do
    begin
      logFilesExist:=true;
      comboLogFiles.AddItem(SQLQuery1.FieldByName('filename').AsString, TLogInfo.Create(SQLQuery1.FieldByName('size').AsLongint));
      SQLQuery1.Next;
    end;
  //show the first file
  if comboLogFiles.Items.Count>0 then
    comboLogFiles.ItemIndex:=0;
  SQLQuery1.Close;
  SQLTransaction1.Commit;
  comboLogFiles.Enabled:=true;
  result:=logFilesExist;
end;


procedure TfrmMain.setTimerInterval;
var
  interval:integer;
begin
  case comboRefreshInterval.ItemIndex of
   0:  interval:=1;
   1:  interval:=5;
   2:  interval:=10;
   3:  interval:=30;
  end;
  timer.Interval:=interval*1000;
end;

procedure TfrmMain.setValues;
begin
  DBConnection.Params.Clear;
  DBConnection.HostName:=edHost.Text;
  DBConnection.Params.Add('port=' + inttostr(sePort.Value));
  DBConnection.Params.Add('application_name=' + APPLICATION_NAME);
  DBConnection.UserName:=edUser.Text;
  DBConnection.Password:=edPassword.Text;
  DBConnection.DatabaseName:='postgres';
end;

procedure TfrmMain.emptyLogCombo;
begin
  while(comboLogFiles.Items.Count>0) do
    begin
      if assigned(comboLogFiles.Items.Objects[comboLogFiles.Items.Count-1]) then
        TLogInfo(comboLogFiles.Items.Objects[comboLogFiles.Items.Count-1]).Free;
      comboLogFiles.Items.Delete(comboLogFiles.Items.Count -1);
    end;
  //seems that's not enough to clear the display
  comboLogFiles.Clear;
end;

procedure TfrmMain.timerTimer(Sender: TObject);
begin
  getLogRecords;
end;

procedure TfrmMain.btnClearClick(Sender: TObject);
begin
  clearList;
end;

procedure TfrmMain.btnStartClick(Sender: TObject);
begin
  Assert(myState in [READY, RUNNING, PAUSED]);
  case myState of
    READY, PAUSED:
      setState(RUNNING);
    RUNNING:
      setState(PAUSED);
  end;
end;

procedure TfrmMain.comboRefreshIntervalChange(Sender: TObject);
begin
  setTimerInterval;
end;

procedure TfrmMain.FormClose(Sender: TObject; var CloseAction: TCloseAction);
begin
  CloseAction:=caFree;
  emptyLogCombo;
  Parser.Free;
end;

procedure TfrmMain.FormCreate(Sender: TObject);
begin
  caption := APPLICATION_NAME;
  parser:= TCSVParser.Create;
  setState(Disconnected);
end;

procedure TfrmMain.getLogRecords;
  procedure maybeAddItem(listItem:TListItem; excludeSelf:boolean);
  var
    finalSubItem:string;
    selfGenerated:boolean;
  begin
    if (listItem<>nil) then
      begin
        finalSubItem:= listItem.SubItems.Strings[listItem.SubItems.Count-1];
        selfGenerated := (finalSubItem = APPLICATION_NAME);
        if (not selfGenerated) then
          listView.Items.AddItem(listItem)
        else
          if (not excludeSelf) then
            listView.Items.AddItem(listItem)
          else
            //listView isn't responsible as listItem hasn't been added
            listItem.Free;
      end;
  end;
var
  sql:String;
  fileText:String;
  fileLines:TStrings;
  logFileName:String;
  currentParserRow:integer;
  currentListItem:TListItem = nil;
  bytesRead:integer;
  newLastBytesRead:integer;
begin
  LogFileName:='pg_log/'+  comboLogFiles.Text;
  SQL:=Format('select pg_read_file(''%s'',%d, %d)', [LogFileName, lastBytesRead, MAX_FILE_SIZE]);
  SQLQuery1.SQL.Text:= sql;
  try
    SQLTransaction1.StartTransaction;
    SQLQuery1.Open;
    //there should only be one record
    if (not SQLQuery1.EOF) then
      begin
        FileText:=  SQLQuery1.FieldByName('pg_read_file').AsString;
        bytesRead := Length(FileText);
        newLastBytesRead := lastBytesRead +  bytesRead;
        statusBar.SimpleText:=Format('%d bytes Read: %d -> %d', [bytesRead, lastBytesRead, newLastBytesRead]);
        lastBytesRead  := newLastBytesRead;
      end;
    SQLQuery1.Close;
    SQLTransaction1.Commit;
  except
    on e:EDatabaseError do
      begin
        statusBar.SimpleText:= e.Message;
        exit;
      end;
  end;
  //sorry! we are going to have to throw away the first lines until whatever's left
  //won't exceed MAX_LOG_LINES, probably will only occur on first read
  fileLines := TStringList.Create;
  fileLines.Text:=fileText;
  while (fileLines.Count>MAX_LOG_LINES) do
    fileLines.Delete(0);
  parser.SetSource(fileLines.Text);
  parser.ResetParser;
  currentParserRow:= -1;
  while parser.ParseNextCell do
    begin
      if (parser.CurrentRow>currentParserRow) then
        begin
          //add the previous currentListItem to the TListView
          if (currentListItem <> nil) then
            begin
              //don't let the list get too long
              while (listView.Items.Count>MAX_LOG_LINES) do
                listView.Items.Delete(0);
              maybeAddItem(currentListItem, ckExcludeSelf.Checked);
            end;
          //and now get a new one ready
          currentListItem:= TListItem.Create(listView.Items);
          currentParserRow:=Parser.CurrentRow;
          currentListItem.Caption:=Parser.CurrentCellText;
        end
      else
        if (parser.CurrentCol<listView.ColumnCount) then
          currentListItem.SubItems.Add(parser.CurrentCellText);
    end;
  //parser may have no more cells left & there is a TListItem not yet added
  maybeAddItem(currentListItem, ckExcludeSelf.Checked);
  fileLines.Free;
  //scroll to bottom of listview
  if (listview.items.count>0) then
    listview.items[listview.items.count-1].makevisible(false);
end;

procedure TfrmMain.toggleConnection(Sender: TObject);
begin
  case myState of
   DISCONNECTED:
     setState(READY);
   else
     setState(DISCONNECTED);
  end;
end;


procedure TfrmMain.setState(newState: State);
begin
  if(myState <> newState) then
    begin
      case newState of
         DISCONNECTED:
           begin
             timer.Enabled:=false;
             DBConnection.Connected:=false;
             emptyLogCombo;
             btnConnect.Hint:='Connect';
             btnConnect.Down:=false;
             btnStart.Enabled:=false;
             firstRun:=true;
             statusBar.SimpleText:= 'Disconnected';
             myState := DISCONNECTED;
           end;
         READY:
           begin
             Assert(myState = DISCONNECTED);
             clearList;
             lastBytesRead:=0;
             setValues;
             try
               DBConnection.Open;
               if DBConnection.Connected then
                 begin
                   statusBar.SimpleText:= 'Connected ok';
                   if populateFileList then
                     begin
                       btnConnect.Hint:='Disconnect';
                       btnConnect.Down:=true;
                       btnStart.Enabled:=true;
                       btnStart.Hint:='Run';
                       btnStart.Down:=false;
                       myState:=READY;
                     end
                   else
                     begin
                       statusBar.SimpleText:= 'No server side csv log files found';
                       DBConnection.Close(true);
                       btnConnect.Down:=false;
                       myState:=DISCONNECTED;
                     end;
                 end;
             except
               on e:EDatabaseError do
                 //myState remains unchanged
                 statusBar.SimpleText:= e.Message;
             end;
           end;
         RUNNING:
           begin
             Assert(myState in [READY, PAUSED]);
             case myState of
               READY:
                 begin
                   comboLogFiles.Enabled:=false;
                   lastBytesRead := TLogInfo(comboLogFiles.Items.Objects[comboLogFiles.ItemIndex]).logsize;
                   setTimerInterval;
                   btnStart.Hint:='Pause';
                   btnStart.Down:=true;
                   myState:=RUNNING;
                   timer.Enabled:=true;
                 end;
               PAUSED:
                 begin
                   //just the same, but lastBytesRead isn't reset
                   setTimerInterval;
                   btnStart.Hint:='Pause';
                   btnStart.Down:=true;
                   myState:=RUNNING;
                   timer.Enabled:=true;
                 end;
             end;
             myState:=RUNNING;
           end;
         PAUSED:
           begin
             Assert(myState=RUNNING);
             timer.Enabled:=false;
             btnStart.Hint:='Resume';
             btnStart.Down:=false;
             myState:=PAUSED;
           end;
      end;
    end;
end;

procedure TfrmMain.clearList;
begin
  listView.Clear;
end;



end.

