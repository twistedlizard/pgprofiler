unit loginfo;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils;

type

{ Tlog }

TLogInfo = class
  logsize:Longint;
public
  constructor Create(size:Longint);
end;

implementation

{ Tlog }

constructor TLogInfo.Create(size: Longint);
begin
  logsize:=size;
end;

end.

