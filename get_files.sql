﻿SELECT f.filename, s.size
FROM pg_ls_dir('pg_log') AS f(filename),
     LATERAL pg_stat_file('pg_log/' || f.filename) as s
where
  f.filename like '%.csv'
order by
  s.modification desc